from flask import Flask, render_template, json, url_for, redirect, session, request
import redis

redis_host = "127.0.0.1"
redis_port = 6379
redis_password = ""

app = Flask(__name__)

@app.route('/')
def home():
	return redirect('dictionary')

@app.route('/dictionary')
def dictionaryCustomer():
	r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)
	firstName = r.lrange("firstName", 0, -1)
	lastName = r.lrange("lastName", 0, -1)
	phone = r.lrange("phone", 0, -1)
	address = r.lrange("address", 0, -1)
	gender = r.lrange("gender", 0, -1)
	birthday = r.lrange("birthday", 0, -1)
	idCustomer = r.lrange("idCustomer", 0, -1)

	dataCustomer = zip(firstName, lastName, phone, address, gender, birthday, idCustomer)

	return render_template('dictionary.html', dataCustomer=dataCustomer)

@app.route('/landingpage')
def landingpage():

	landingpage = ''
	return render_template('landingpage.html', landingpage=landingpage)

@app.route('/dictionary', methods=['POST'])
def insertCustomer():
	data = request.form
	firstName = data['firstName']
	lastName = data['lastName']
	phone = data['phone']
	address = data['address']
	gender = data['gender']
	birthday = data['birthday']
	r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)

	r.lpush("firstName", firstName)
	r.lpush("lastName", lastName)
	r.lpush("phone", phone)
	r.lpush("address", address)
	r.lpush("gender", gender)
	r.lpush("birthday", birthday)

	idL = r.llen("firstName")
	r.lpush("idCustomer", int(idL))
	return redirect('dictionary')


@app.route('/updateCustomer', methods=['POST'])
def updateCustomer():
	data = request.form
	idCustomer = data['idUpdate']
	firstName = data['firstNameUpdate']
	lastName = data['lastNameUpdate']
	phone = data['phoneUpdate']
	address = data['addressUpdate']
	gender = data['genderUpdate']
	birthday = data['birthdayUpdate']
	r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)

	index = r.lindex("idCustomer",idCustomer)
	kode = str(index)

	if kode == "None":
		r.lset("firstName",0, firstName)
		r.lset("lastName",0, lastName)
		r.lset("phone",0, phone)
		r.lset("address",0, address)
		r.lset("gender",0, gender)
		r.lset("birthday",0, birthday)
		r.lset("idCustomer",0, idCustomer)

		return redirect('dictionary')
	else:
		r.lset("firstName",kode, firstName)
		r.lset("lastName",kode, lastName)
		r.lset("phone",kode, phone)
		r.lset("address",kode, address)
		r.lset("gender",kode, gender)
		r.lset("birthday",kode, birthday)
		r.lset("idCustomer",kode, idCustomer)
		return redirect('dictionary')

@app.route('/delete', methods=['POST'])
def deleteCustomer():
	idCustomer = request.form['idRemove']
	r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)

	# index = r.lindex("idCustomer", idCustomer)

	firstName = request.form['firstNameRemove']
	lastName = request.form['lastNameRemove']
	phone = request.form['phoneRemove']
	address = request.form['addressRemove']
	gender = request.form['genderRemove']
	birthday = request.form['birthdayRemove']



	r.lrem('firstName', -1, firstName)
	r.lrem('lastName', -1, lastName)
	r.lrem('phone', -1, phone)
	r.lrem('address', -1, address)
	r.lrem('gender', -1, gender)
	r.lrem('birthday', -1, birthday)
	r.lrem('idCustomer', -1, idCustomer)

	return redirect('dictionary')

if __name__ == '__main__':
	app.run(debug=True)
